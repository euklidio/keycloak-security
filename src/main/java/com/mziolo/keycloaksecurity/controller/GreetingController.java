package com.mziolo.keycloaksecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class GreetingController {

    @GetMapping("/doctor")
    public Mono<String> greeting() {

        return Mono.just("Hello doctor");
    }

    @GetMapping("/patient")
    public Mono<String> greeting2() {

        return Mono.just("Hello patient");
    }

    @GetMapping("/all")
    public Mono<String> greeting3() {

        return Mono.just("Hello patient or doctor");
    }
}
