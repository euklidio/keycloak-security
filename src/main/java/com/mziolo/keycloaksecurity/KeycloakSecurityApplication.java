package com.mziolo.keycloaksecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Configuration;



@SpringBootApplication
@Configuration
public class KeycloakSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakSecurityApplication.class, args);
	}


}
